<div class="blog-masthead">
    <div class="container">
        <nav class="nav blog-nav">
            <a class="nav-link active" href="/#">Home</a>
            @if (Auth::check())
            <a class="nav-link" href="/posts/create">Create Post</a>
            <a class="nav-link" href="#">{{ Auth::user()->name }}</a>
            <a class="nav-link" href="/logout">Log out</a>
            @else
            <a class="nav-link" href="/register">Register</a>
            <a class="nav-link" href="/login">Log in</a>
            @endif
        </nav>
    </div>
</div>
