<div class="col-sm-3 offset-sm-1 blog-sidebar">
    @include ("layouts.about")
    @include ("layouts.archive")
    @include ("layouts.tags")
    @include ("layouts.elsewhere")
</div><!-- /.blog-sidebar -->
