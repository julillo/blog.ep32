<?php

namespace App\Providers;

use App\Billing\Stripe;
use App\Post;
use App\Tag;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use function config;
use function view;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        Schema::defaultStringLength(191);

        view()->composer("layouts.sidebar", function($view) {
            $archives = Post::archives();
            $tags = Tag::has("posts")->pluck("name");
            $view->with(compact("archives", "tags"));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
//        \App::singleton("App\Billing\Stripe", function() {
        $this->app->singleton(Stripe::class, function() {

            return new Stripe(config("services.stripe.secret"));
        });
    }

}
