<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Mail\Welcome;

class RegistrationForm extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            "name" => "required|max:191"
            , "email" => "required|email|max:191"
            , "password" => "required|confirmed"
        ];
    }

    public function persist() {
        //create and save the user

        $user = User::create([
                    'name' => $this->name,
                    'email' => $this->email,
                    'password' => bcrypt($this->password)
        ]);

        //sign them in

        auth()->login($user);

        // send a welcome mail

        Mail::to($user)->send(new Welcome($user));
    }

}
