<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionsController extends Controller {

    public function __construct() {
        $this->middleware("guest", ["except" => "destroy"]);
    }

    public function create() {
        return view("sessions.create");
    }

    public function store() {

        // Attempt to authenticate the user

        if (!auth()->attempt(request(["email", "password"]))) {
            // if not, redireck back
            return back()->withErrors([
                        "message" => "Plase check your credentials and try again."
            ]);
        }
        //if so, sign them in
        //redirect to the home page.

        return redirect()->home();
    }

    public function destroy() {
        auth()->logout();

        return redirect()->home();
    }

}
