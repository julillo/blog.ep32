<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Repositories\Posts;

class PostsController extends Controller {

    public function __construct() {
        $this->middleware("auth")->except(["index", "show"]);
    }

    public function index(Posts $posts) {

// <editor-fold defaultstate="collapsed" desc="ep anteriores">
//        dd($posts);
//        $posts = (new \App\Repositories\Posts)->all();
//        $posts = Post::latest()
//                ->filter(request(["month", "year"]))
//                ->get();
// </editor-fold>
        
//        return session("message");
        
        $posts = $posts->all();
        
        return view("posts.index")->with("posts", $posts);
    }

    public function show(Post $post) {

        return view("posts.show", compact("post"));
    }

    public function create() {
        return view("posts.create");
    }

    public function store() {

        $this->validate(request(), [
            "title" => "required|max:191"
            , "body" => "required"
        ]);

        auth()->user()->publish(
                new Post(request(["title", "body"]))
        );
        
        session()->flash("message", "Your post has now been published.");

        return redirect("/");
    }

}
