<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegistrationForm;

class RegistrationController extends Controller {

    public function __construct() {
        $this->middleware("guest"); //->except(["create"]);
    }

    public function create() {
        return view("registration.create");
    }

    public function store(RegistrationForm $form) {


        $form->persist();

//        session("message", "Here is a default message");
        session()->flash("message", "Thanks so much for signing up!");

        return redirect()->home();
    }

}
